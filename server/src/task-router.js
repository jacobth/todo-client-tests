// @flow
import express from 'express';
import taskService, { type Task } from './task-service';

/**
 * Express router containing task methods.
 */
const router: express$Router<> = express.Router();

router.get('/tasks', (request, response) => {
  taskService
    .getAll()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

router.get('/tasks/:id', (request, response) => {
  const id = Number(request.params.id);
  taskService
    .get(id)
    .then((task) => (task ? response.send(task) : response.status(404).send('Task not found')))
    .catch((error: Error) => response.status(500).send(error));
});

// Example request body: { title: "Ny oppgave" }
// Example response body: { id: 4 }
router.post('/tasks', (request, response) => {
  const data = request.body;
  if (data && typeof data.title == 'string' && data.title.length != 0)
    taskService
      .create(data.title, data.descr)
      .then((id) => response.send({ id: id }))
      .catch((error: Error) => response.status(500).send(error));
  else response.status(400).send('Missing task title');
});

router.delete('/tasks/:id', (request, response) => {
  taskService
    .delete(Number(request.params.id))
    .then((result) => response.send())
    .catch((error: Error) => response.status(500).send(error));
});

// router.put('/tasks/:id', (request, response) => {
//   taskService
//     .put(Number(request.params.id))
//     .then((result) => response.send())
//     .catch((error: Error) => response.status(500).send(error));
// });

router.put('/tasks/:id', (request, response) => {
  const fata = request.body;
  if (
    fata &&
    typeof fata.title == 'string' &&
    fata.title.length != 0 &&
    typeof fata.descr == 'string' &&
    fata.descr.length != 0 &&
    typeof fata.done == 'boolean'
  ) {
    taskService
      .putty(Number(request.params.id), fata.title, fata.descr, fata.done)
      .then((result) => response.send())
      .catch((error: Error) => response.status(500).send(error));
  } else {
    response.status(400).send('Wrong innput');
  }
});

export default router;
