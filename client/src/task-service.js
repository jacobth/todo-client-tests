// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Task = {
  id: number,
  title: string,
  descr: string,
  done: boolean,
};

class TaskService {
  /**
   * Get task with given id.
   */
  get(id: number) {
    return axios.get<Task>('/tasks/' + id).then((response) => response.data);
  }

  /**
   * Get all tasks.
   */
  getAll() {
    return axios.get<Task[]>('/tasks').then((response) => response.data);
  }

  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  create(title: string, descr: string) {
    return axios
      .post<{}, { id: number }>('/tasks', { title: title, descr: descr })
      .then((response) => response.data.id);
  }

  updateAll(id: number, title: string, descr: string, done: boolean) {
    console.log(id, title, descr, done);
    return axios.put<{ id: number }>('/tasks/' + id, {
      id: id,
      title: title,
      descr: descr,
      done: done,
    });
  }
  delete(id: number) {
    return axios.delete<{ id: number }>('/tasks/' + id, { id: id });
  }
}

const taskService = new TaskService();
export default taskService;
