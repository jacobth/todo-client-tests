// @flow

import * as React from 'react';
import { TaskList, TaskNew, TaskDetails, TaskEdit } from '../src/task-components';
import { type Task } from '../src/task-service';
import { ReactWrapper, shallow } from 'enzyme';
import { Form, Button, Column, Card, Row } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        { id: 1, title: 'Les leksjon', descr: 'mye arbeid', done: false },
        { id: 2, title: 'Møt opp på forelesning', descr: 'noe', done: false },
        { id: 3, title: 'Gjør øving', descr: 'noe', done: false },
      ]);
    }
    get() {
      return Promise.resolve({ id: 1, title: 'Les leksjon', descr: 'mye arbeid', done: false });
    }

    create(title: string) {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }
    delete(id: number) {
      return Promise.resolve();
    }
  }

  return new TaskService();
});

describe('Task component tests', () => {
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

  /////////////////////////////////////////////////////////////////___________________________////////////////////////7

  test('TaskDetails draws correctly', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <Column width={2}>Title:</Column>,
          <Column>Les leksjon</Column>,
          <Column width={2}>Description:</Column>,
          <Column>mye arbeid</Column>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('TaskDetails draws correctly', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Wait for events to complete
    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  });

  test.skip('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

  test('Butt1', (done) => {
    const wrapper = shallow(<TaskList />);
    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/new');
      done();
    });
  });

  test('Input updates correctly', () => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);
    // $FlowExpectedError: do not type check next line.
    expect(
      wrapper.containsAllMatchingElements([
        <Form.Input type="text" value="" />,
        <Form.Textarea value="" rows={10} />,
        <Form.Checkbox checked={false} />,
      ])
    );
    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test' } });
    wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'testDescription' } });
    wrapper.find(Form.Checkbox).simulate('change', { currentTarget: { checked: true } });

    // $FlowExpectedError: do not type check next line.
    expect(wrapper.containsMatchingElement(<Form.Input value="test" />));
    expect(wrapper.containsMatchingElement(<Form.Textarea value="testDescription" />));
    expect(wrapper.containsMatchingElement(<Form.Checkbox checked={true} />));
  });

  test.skip('TaskList points correctly correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    setTimeout(() => {
      console.log(wrapper.debug());
      expect(wrapper.containsMatchingElement(<NavLink to="/tasks/1">Les leksjon</NavLink>)).toEqual(
        true
      );
      done();
    });

    // wrapper.find(NavLink).at(0).simulate('click');
    // // Wait for events to complete
    // setTimeout(() => {
    //   expect(location.hash).toEqual('#/tasks/1');
    //   done();
    // });
  });

  test('Input updates correctly', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);
    // $FlowExpectedError: do not type check next line.
    wrapper.find(Button.Danger).simulate('click');

    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/');
      done();
    });
  });
});
